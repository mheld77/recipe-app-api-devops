variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "mheld.dev@gmail.com"
}

variable "db_username" {
  description = "User for RDS postgres instance"
}

variable "db_password" {
  description = "Password for RDS postgres instance"
}

variable "bastion_key_name" {
  default = "receip-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "017757212032.dkr.ecr.us-east-2.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "017757212032.dkr.ecr.us-east-2.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
