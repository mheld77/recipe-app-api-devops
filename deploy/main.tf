terraform {
  backend "s3" {
    bucket         = "mheld-app-api-devops-tfstate-01"
    key            = "recipe-app.tfstate"
    region         = "us-east-2"
    encrypt        = true
    dynamodb_table = "mheld-app-api-devops-tfstate-01-lock"
  }
}

provider "aws" {
  version = "~> 2.54.0"
  region  = "us-east-2"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {

}